package com.example.pos_flutter

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.annotation.NonNull
import com.google.gson.Gson
import com.kishcore.sdk.hybrid.api.SDKManager
import com.kishcore.sdk.hybrid.api.WaterMarkLanguage
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel
import com.example.pos_flutter.models.PaymentResult
import com.example.pos_flutter.models.PaymentData
import com.example.pos_flutter.enums.PaymentStatus
import com.example.pos_flutter.enums.ReceiptType
import org.json.JSONException
import org.json.JSONObject


class MainActivity: FlutterActivity() {
    private val CHANNEL = "com.example.pos_flutter/pos"
    private var methodChannel: MethodChannel? = null

    private var paymentResult: MethodChannel.Result? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        SDKManager.init(MainActivity@this)

    }

    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)
        methodChannel = MethodChannel(flutterEngine.dartExecutor.binaryMessenger, CHANNEL)
        methodChannel?.setMethodCallHandler {
                call, result ->
            if (call.method == "printBitmap") {
                val bitmapByteArray = call.argument<ByteArray>("bitmapByteArray")
                bitmapByteArray?.let {
                    val bmp = BitmapFactory.decodeByteArray(it, 0, it.size)
                    val printStatus = print(MainActivity@this, bmp)
                    result.success(printStatus)
                } ?: run {
                    result.notImplemented()
                }
            } else if (call.method == "payment") {
                val amount = call.argument<String>("amount")
                amount?.let {
                    payment(amount)
                    paymentResult = result
                } ?: run {
                    result.notImplemented()
                }
            } else {
                result.notImplemented()
            }
        }
    }

    private fun print(context: Context?, bmp: Bitmap?) {
        if (SDKManager.getPrinterStatus() == SDKManager.STATUS_OK) {
            SDKManager.printBitmap(
                context,
                bmp,
                true,
                70,
                WaterMarkLanguage.ENGLISH,
                true
            ) { data ->
                showToast(
                    "پایان پرینت"
                )
            }
        } else {
            showToast("پرینتر با مشکل مواجه شده است.")
        }
    }

    private fun payment(amount: String) {
        val packageName: String = Const.NOVIN_POS_PACKAGE_NAME
        if (!isPackageInstalled(packageName)) {
            showToast("اپلیکیشن نوین پوز نصب نمی باشد.")
            return
        }

        val intent = Intent(Const.PAYMENT_INTENT)
        intent.setPackage(packageName)
        val bundle = Bundle()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("AndroidPosMessageHeader", "@@PNA@@")
            jsonObject.put("ECRType", "1")
            jsonObject.put("Amount", amount)
            jsonObject.put("TransactionType", "00")

            //optional
            jsonObject.put("BillNo", "123456")
            jsonObject.put("AdditionalData", "123456")
            jsonObject.put("OriginalAmount", amount)
            jsonObject.put("SwipeCardTimeout", "30000")
            jsonObject.put("ReceiptType", "1") // customer and merchant:1 / customer only:2
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        bundle.putString("Data", jsonObject.toString());
        intent.putExtras(bundle)
        startActivityForResult(intent, Const.REQUEST_CODE_FOR_PAYMENT)
    }
    
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            if (requestCode == Const.REQUEST_CODE_FOR_PAYMENT) {
                val bundle = data!!.extras
                bundle?.let {
                    val resultStr = bundle.getString("Result")
                    resultStr?.let {
                        paymentResult?.success(resultStr)
                    } ?: run {
                        showToast("پرداخت با مشکل مواجه شده است.")
                    }
                }
            }
        }
    }

    private fun isPackageInstalled(packageName: String): Boolean {
        return try {
            packageManager.getPackageInfo(packageName, 0)
            true
        } catch (e: PackageManager.NameNotFoundException) {
            false
        }
    }

    private fun showToast(message: String) {
        Toast.makeText(this@MainActivity, message, Toast.LENGTH_SHORT).show()
    }

    override fun onStop() {
        super.onStop()
        SDKManager.closePrinter()
    }
}
